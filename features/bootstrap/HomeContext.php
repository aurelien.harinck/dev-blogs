<?php

use Behat\MinkExtension\Context\MinkContext;

class HomeContext extends MinkContext {
    /**
     * @Given I am on the moon
     */
    public function iAmOnTheMoon()
    {
       $this->visitPath('/');
    }
}
